//Khai báo thư viện express
const express = require("express");
const { courseMiddleWare, getAllCoursesMiddleWare, getCourseByIdMiddleWare, putCourseByIdMiddleWare, deleteCourseByIdMiddleWare, postCoursesMiddleWare } = require("../middlewares/courseMiddleware");

//Khai báo router
const courseRouter = express.Router();

//Middleware chung cho cả router
courseRouter.use(courseMiddleWare);

//Get all course
courseRouter.get("/courses", getAllCoursesMiddleWare, (req, res) => {
    console.log("Get All Courses")
    res.json({
        message: "Get All Courses"
    })
});

//Get a course by id
courseRouter.get("/courses/:courseId", getCourseByIdMiddleWare, (req, res) => {
    let courseId = req.params.courseId;
    console.log("Get CourseId = " + courseId)
    res.json({
        message: "Get CourseId = " + courseId
    })
});

//Create new course
courseRouter.post("/courses", postCoursesMiddleWare, (req, res) => {    
    console.log("Create new course");
    var body = req.body;
    console.log(body);
    res.json({
        message: "Create new course",
        body
    })
});

//Update course by id
courseRouter.put("/courses/:courseId", putCourseByIdMiddleWare, (req, res) => {
    let courseId = req.params.courseId;
    console.log("Update CourseId = " + courseId)
    res.json({
        message: "Update CourseId = " + courseId
    })
});

//Delete course by id
courseRouter.delete("/courses/:courseId", deleteCourseByIdMiddleWare, (req, res) => {
    let courseId = req.params.courseId;
    console.log("Delete CourseId = " + courseId)
    res.json({
        message: "Delete CourseId = " + courseId
    })
});

//Export module
module.exports = { courseRouter }