//Khai báo thư viện express
const express = require("express");
const { reviewMiddleWare, getAllReviewsMiddleWare, getReviewByIdMiddleWare, putReviewByIdMiddleWare, deleteReviewByIdMiddleWare, postReviewsMiddleWare } = require("../middlewares/reviewMiddleware");

//Khai báo router
const reviewRouter = express.Router();

//Middleware chung cho cả router
reviewRouter.use(reviewMiddleWare);

//Get all review
reviewRouter.get("/reviews", getAllReviewsMiddleWare, (req, res) => {
    console.log("Get All reviews")
    res.json({
        message: "Get All reviews"
    })
});

//Get a review by id
reviewRouter.get("/reviews/:reviewId", getReviewByIdMiddleWare, (req, res) => {
    let reviewId = req.params.reviewId;
    console.log("Get ReviewId = " + reviewId)
    res.json({
        message: "Get ReviewId = " + reviewId
    })
});

//Create new review
reviewRouter.post("/reviews", postReviewsMiddleWare, (req, res) => {    
    console.log("Create new review");
    var body = req.body;
    console.log(body);
    res.json({
        message: "Create new review",
        body
    })
});

//Update review by id
reviewRouter.put("/reviews/:reviewId", putReviewByIdMiddleWare, (req, res) => {
    let reviewId = req.params.reviewId;
    console.log("Update reviewId = " + reviewId)
    res.json({
        message: "Update reviewId = " + reviewId
    })
});

//Delete review by id
reviewRouter.delete("/reviews/:reviewId", deleteReviewByIdMiddleWare, (req, res) => {
    let reviewId = req.params.reviewId;
    console.log("Delete reviewId = " + reviewId)
    res.json({
        message: "Delete reviewId = " + reviewId
    })
});

//Export module
module.exports = { reviewRouter }