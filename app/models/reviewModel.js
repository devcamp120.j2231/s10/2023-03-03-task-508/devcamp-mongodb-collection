const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const reviewSchema = new Schema({
    // Trường _id mặc định tự tạo trong CSDL
    stars: {
        type: Number,
        default: 0
    },
    note: {
        type: String,
        required: false
    }
}, {
    // Tham số tạo ra các trường đặt lịch sử tương tác với bản ghi
    timestamps: true
});

module.exports = mongoose.model("review", reviewSchema);